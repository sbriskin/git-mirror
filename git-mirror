#!/usr/bin/env bash
#
# git-mirror
# Copyright (C) 2020-2021  Sergey Briskin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

APP="$(basename "$0")"

function show_version(){
    echo "Version: 1.0 (2103)"
    exit 0
}

function show_help() {
cat<<EOT

$APP - Easy repo mirroring from one remote git to any other

Usage:
    $APP --from="https://user:token@git.example.com/project/repo.git" --to="http://user:pass@git.corp.net/project/repo.git"

EOT
exit 0
}

function mirror() {
    if [[ -d "$HOME/tmp" ]]; then
        TMPDIR="$HOME/tmp"
    fi
    WORKDIR="$(mktemp -d "${TMPDIR:-/tmp}/$(basename "$0")-XXXXXX")"
    cd "${WORKDIR}" || exit 1
    REPO="${SOURCE##*/}"
    REPO_NAME="${REPO//'.git'*/}"

    echo "Mirroring ${REPO_NAME}"
    echo "=================================================="
    echo "FROM: ${SOURCE}"
    echo "TO:   ${DESTINATION}"
    echo "=================================================="

    git clone --mirror "${SOURCE}"
    cd "${REPO}" || exit 1
    git push  --mirror "${DESTINATION}"
    cd ..
    rm -rf "${REPO}" || exit 1
    rm -rf "${WORKDIR}"
}

for ARG in "${@}"; do
    case $ARG in
        --from=* )
            SOURCE=${ARG##*=}
            ;;
        --to=* )
            DESTINATION=${ARG##*=}
            ;;
        --version|-v|version)
            show_version
            ;;
        --help|-h|help|*)
            show_help
            ;;
    esac
done

if [[ "$SOURCE" && "$DESTINATION" ]]; then
    mirror
else
    show_help
fi

exit
